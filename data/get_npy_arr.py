import numpy as np
#from keras.preprocessing.image import array_to_img, img_to_array, load_img
import numpy as np
import cv2
from os import walk 

#folder to walk and search
# my_path = '/PetImages'

#placeholders for filenames and directory names
fs1 = []
fs2 = []

dirs = ['Cat', 'Dog']

cx1_imgs = []
cx1_labels = []

dx2_imgs =[]
dx2_labels = []


## walks the given directory and gets the image filenames
for (dirpath1, dirnames1, filenames1) in walk(dirs[0]):
    fs1.extend(filenames1)

for (dirpath2, dirnames2, filenames2) in walk(dirs[1]):
    fs2.extend(filenames2)

no1 =len(np.array(fs1))
no2 =len(np.array(fs2))


## Reads the image files in the given dir and appends the numpy arrays into the list
for i in range(no1):
    x= str(dirs[0] + '/' + fs1[i])
    # img = load_img(x)
    img = cv2.imread(x)
    img = cv2.resize(img , (32, 32))
    
    cx1_imgs.append(np.array(img))
    cx1_labels.append(np.array([0], dtype=np.uint8))

cx1_imgs = np.array(cx1_imgs)
cx1_labels = np.array(cx1_labels)


for i in range(no2):
    y = str(dirs[0] + '/' + fs2[i])
    # img = load_img(x)
    img = cv2.imread(x)
    img = cv2.resize(img , (32, 32))

    dx2_imgs.append(np.array(img))
    dx2_labels.append(np.array([1], dtype=np.uint8))


dx2_imgs = np.array(dx2_imgs)
dx2_labels = np.array(dx2_labels)

print(cx1_imgs.shape)
print(cx1_labels.shape)

img_save = np.append(cx1_imgs, dx2_imgs, axis = 0)
print(img_save.shape)
label_save = np.append(cx1_labels, dx2_labels, axis = 0)
print(label_save.shape)

#shuffle arrays

rng_state = np.random.get_state()
np.random.shuffle(img_save)
np.random.set_state(rng_state)
np.random.shuffle(label_save)

#save np arrays

np.save('imgs', img_save )
np.save('labels', label_save )
