import os
import tensorflow as tf
from tensorflow.keras.datasets import cifar10
from convert import encode
import numpy as np

def save_training_data(images, labels, filename):
    assert images.shape[0] == labels.shape[0]
    num_examples = images.shape[0]

    with tf.python_io.TFRecordWriter(filename) as writer:

        for index in range(num_examples):

            image = images[index]
            label = labels[index]
            example = encode(image, label)
            writer.write(example.SerializeToString())


# (x_train, y_train), (x_test, y_test) = cifar10.load_data()
# print(x_test.shape)

imgs = np.load('data/imgs.npy')
labels = np.load('data/labels.npy')

(x_train, y_train) = (imgs[:20000], labels[:20000])
(x_test, y_test) = (imgs[20000:], labels[20000:])


data_dir = os.path.expanduser("./data/")

if not os.path.exists(data_dir):
    os.makedirs(data_dir)

save_training_data(x_train, y_train, os.path.join(data_dir, "train.tfrecord"))
save_training_data(x_test, y_test, os.path.join(data_dir, "test.tfrecord"))